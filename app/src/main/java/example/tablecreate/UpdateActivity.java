package example.tablecreate;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class UpdateActivity extends AppCompatActivity {

    EditText nameText, dobText, emailText, mobileText;
    String nameVal, dobVal, emailVal, mobVal;
    int idval;
    Button updateBtn;
    DBHelper dbHelper;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update);

        idval = getIntent().getIntExtra("id", 0);
        nameVal = getIntent().getStringExtra("name");
        dobVal = getIntent().getStringExtra("dob");
        emailVal = getIntent().getStringExtra("email");
        mobVal = getIntent().getStringExtra("mobile");

        nameText = (EditText) findViewById(R.id.nameEdit);
        dobText = (EditText) findViewById(R.id.dobEdit);
        emailText = (EditText) findViewById(R.id.emailIdEdit);
        mobileText = (EditText) findViewById(R.id.mobEdit);

        nameText.setText(nameVal);
        dobText.setText(dobVal);
        emailText.setText(emailVal);
        mobileText.setText(mobVal);

        dbHelper = new DBHelper(this);

        updateBtn = (Button) findViewById(R.id.subBtn);
        updateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                nameVal = nameText.getText().toString();
                dobVal = dobText.getText().toString();
                emailVal = emailText.getText().toString();
                mobVal = mobileText.getText().toString();

                String query="update Student set Name ='" + nameVal + "',";
                query += "DOB='" + dobVal + "',";
                query += "emailId='" + emailVal + "',";
                query += "MobileNum ='" + mobVal + "'";
                query += " where id = " + idval ;

                dbHelper.executeQuery(query);

                startActivity(new Intent(UpdateActivity.this, DisplayActivity.class));
                finish();
            }
        });
    }
}
