package example.tablecreate;

import android.Manifest;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.Map;

public class MainActivity extends AppCompatActivity {

    EditText nameText, dobText, emailText, mobileText;
    String nameVal, dobVal, emailVal, mobVal;
    Button subBtn;
    DBHelper dbHelper;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        nameText = (EditText) findViewById(R.id.nameEdit);
        dobText = (EditText) findViewById(R.id.dobEdit);
        emailText = (EditText) findViewById(R.id.emailIdEdit);
        mobileText = (EditText) findViewById(R.id.mobEdit);

        dbHelper = new DBHelper(this);

        subBtn = (Button) findViewById(R.id.subBtn);

        subBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                nameVal = nameText.getText().toString().trim();
                dobVal = dobText.getText().toString().trim();
                emailVal = emailText.getText().toString().trim();
                mobVal = mobileText.getText().toString().trim();

                String query="insert into Student (Name, DOB, emailId, MobileNum) values (";
                query += "'" + nameVal + "',";
                query += "'" + dobVal + "',";
                query += "'" + emailVal + "',";
                query += "'" + mobVal + "')";

                dbHelper.executeQuery(query);

                Intent intent = new Intent(MainActivity.this, DisplayActivity.class);
                startActivity(intent);
            }
        });
    }
}
