package example.tablecreate;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class StudentAdapter extends RecyclerView.Adapter<StudentAdapter.ViewHolder> {

    DisplayActivity displayActivity;
    List<Student> itemList = new ArrayList<>();

    public StudentAdapter(DisplayActivity displayActivity, List<Student> itemList){

        this.displayActivity = displayActivity;
        this.itemList = itemList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.student_layout, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {

        holder.idText.setText(String.valueOf(itemList.get(position).getId()));
        holder.nameText.setText(itemList.get(position).getName());
        holder.dobText.setText(itemList.get(position).getDob());
        holder.emailText.setText(itemList.get(position).getEmailId());
        holder.mobileText.setText(itemList.get(position).getMobileNum());

        holder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(displayActivity, UpdateActivity.class);
                intent.putExtra("id", itemList.get(position).getId());
                intent.putExtra("name", itemList.get(position).getName());
                intent.putExtra("dob", itemList.get(position).getDob());
                intent.putExtra("email", itemList.get(position).getEmailId());
                intent.putExtra("mobile", itemList.get(position).getMobileNum());

                displayActivity.startActivity(intent);
                displayActivity.finish();
            }
        });
    }


    @Override
    public int getItemCount() {
        return itemList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView idText, nameText, emailText, mobileText, dobText;
        LinearLayout linearLayout;
        public ViewHolder(View itemView) {
            super(itemView);

            idText = (TextView) itemView.findViewById(R.id.id);
            nameText = (TextView) itemView.findViewById(R.id.name);
            emailText =(TextView) itemView.findViewById(R.id.email);
            mobileText = (TextView) itemView.findViewById(R.id.mobile);
            dobText = (TextView) itemView.findViewById(R.id.dob);
            linearLayout =(LinearLayout) itemView.findViewById(R.id.layoutId);
        }
    }
}
