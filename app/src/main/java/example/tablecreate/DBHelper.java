package example.tablecreate;

import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

public class DBHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "FeedbackAdmin.db";
    private static final int VERSION = 1;

    public String Create_student_Table="create table Student"+
            "( Id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"+
            " Name TEXT NOT NULL ,"+
            " DOB TEXT NOT NULL ,"+
            " emailId TEXT NOT NULL,"+
            " MobileNum TEXT NOT NULL)";

    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

        sqLiteDatabase.execSQL(Create_student_Table);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS Student");
    }

    public void executeQuery(String query){

        SQLiteDatabase db = this.getWritableDatabase();
        try {
            db.execSQL(query);
        }catch (Exception e){
            Log.i("TAG", e.toString());
        }
        db.close();

    }

    public List<Student> getAllData(){
        List<Student> itemList = new ArrayList<>();
        String query = "select * from Student";
        SQLiteDatabase db = this.getWritableDatabase();

        Cursor cursor =db.rawQuery(query, null);

        if (cursor != null){
            if (cursor.moveToFirst()){
                do {
                    Student student = new Student();
                    student.setId(cursor.getInt(cursor.getColumnIndex("Id")));
                    student.setName(cursor.getString(cursor.getColumnIndex("Name")));
                    student.setDob(cursor.getString(cursor.getColumnIndex("DOB")));
                    student.setEmailId(cursor.getString(cursor.getColumnIndex("emailId")));
                    student.setMobileNum(cursor.getString(cursor.getColumnIndex("MobileNum")));

                    itemList.add(student);
                }while (cursor.moveToNext());
            }
        }
        return itemList;

    }

}
